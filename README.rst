.. -*- restructuredtext -*-

=========================================
TCI — Tiny Collection of Interpreters ;-)
=========================================

Languages supported: Ada, C/C++, Fortran, Java, Objective-C/C++!

| Usage: ``tci [option] file``
| File extensions supported: ``.adb``, ``.c``, ``.cpp``, ``.cc``, ``.cxx``, ``.c++``, ``.C``, ``.f95``, ``.java``, ``.m``, ``.mm``
| Option: ``--help`` — Display information


|TCI|

.. |TCI| image:: https://bitbucket.org/OPiMedia/tci/raw/cea566950d5717d418befed8561ba03769db43f0/_img/tci_32x32.png


Examples:

* | $ ``tci hello_world.adb``
  | ``Hello real world ! (Ada)``

* | $ ``tci hello_world.c``
  | ``Hello real world ! (C)``

* | $ ``tci hello_world.cpp``
  | ``Hello real world ! (C++)``

* | $ ``tci hello_world.f95``
  | ``Hello real world ! (Fortran 95)``

* | $ ``tci HelloWorld.java``
  | ``Hello real world ! (Java)``

* | $ ``tci hello_world.m``
  | ``Hello real world ! (Objective-C)``

* | $ ``tci hello_world.mm``
  | ``Hello real world ! (Objective-C++)``

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2015, 2016 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png

|



Changes
=======
* 02.00.01 --- October 21, 2016: adapted Fortran call.

* 02.00.00 --- January 2, 2015: **Tiny Collection of Interpreters** ;-)

* 01.00.00 --- January 1st, 2015: **Tiny C/C++ Interpreter** ;-)
